package com.rutershok.quotes;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.rutershok.quotes.fragments.AllQuotesFragment;
import com.rutershok.quotes.fragments.FavoritesFragment;
import com.rutershok.quotes.fragments.MyImagesFragment;
import com.rutershok.quotes.fragments.PremiumFragment;
import com.rutershok.quotes.fragments.QuotesFragment;
import com.rutershok.quotes.fragments.SearchQuotesFragment;
import com.rutershok.quotes.fragments.SettingsFragment;
import com.rutershok.quotes.models.Author;
import com.rutershok.quotes.models.Category;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Fragments;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Share;
import com.rutershok.quotes.utils.Snackbar;

import java.util.ArrayList;

public class DrawerSupport extends AppCompatActivity implements Drawer.OnDrawerItemClickListener {

    Drawer drawer;

    void initializeDrawer() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .addDrawerItems(getItems())
                .withOnDrawerItemClickListener(this)
                .build();
    }

    //Get all drawer menu items
    private IDrawerItem[] getItems() {
        return new IDrawerItem[]{
                new PrimaryDrawerItem().withName(R.string.menu_all_quotes).withIcon(R.drawable.ic_all_quotes).withIdentifier(R.string.menu_all_quotes),
                //Authors
                new ExpandableDrawerItem().withName(R.string.menu_authors).withIcon(R.drawable.ic_authors).withSelectable(false)
                        .withSubItems(getAuthors()).withIdentifier(R.string.menu_authors),
                //Categories
                new ExpandableDrawerItem().withName(R.string.menu_categories).withIcon(R.drawable.ic_categories).withSelectable(false)
                        .withSubItems(getCategories()).withIdentifier(R.string.menu_categories),
                new PrimaryDrawerItem().withName(R.string.search_quotes).withIcon(R.drawable.ic_search).withIdentifier(R.string.search_quotes),
                new PrimaryDrawerItem().withName(R.string.menu_favorites).withIcon(R.drawable.ic_favorite).withIdentifier(R.string.menu_favorites),
                new PrimaryDrawerItem().withName(R.string.menu_my_images).withIcon(R.drawable.ic_my_images).withIdentifier(R.string.menu_my_images),
               // new PrimaryDrawerItem().withName(R.string.menu_text_recognition).withIcon(R.drawable.ic_text_recognition).withIdentifier(R.string.menu_text_recognition),
                new PrimaryDrawerItem().withName(R.string.menu_premium_version).withIcon(R.drawable.ic_premium_version).withIdentifier(R.string.menu_premium_version),
                new PrimaryDrawerItem().withName(R.string.menu_settings).withIcon(R.drawable.ic_settings).withIdentifier(R.string.menu_settings),
                new PrimaryDrawerItem().withName(R.string.menu_exit).withIcon(R.drawable.ic_exit).withIdentifier(R.string.menu_exit).withSelectable(false)};
    }

    //Get all AUTHORS from Data -> Author
    private ArrayList<IDrawerItem> getAuthors() {
        ArrayList<IDrawerItem> authorList = new ArrayList<>();
        for (Author author : Author.values()) {
            authorList.add(new SecondaryDrawerItem()
                    .withName(getResources().getString(author.nameResId))
                    .withLevel(2)
                    .withIdentifier(author.queryResId)); //Choose a single identifier
        }
        return authorList;
    }

    //Get all CATEGORIES from Data -> Category
    private ArrayList<IDrawerItem> getCategories() {
        ArrayList<IDrawerItem> categoriesList = new ArrayList<>();
        for (Category category : Category.values()) {
            categoriesList.add(new SecondaryDrawerItem()
                    .withName(getResources().getString(category.nameResId))
                    .withLevel(2)
                    .withIdentifier(category.queryResId)); //Choose a single identifier
        }
        return categoriesList;
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        int id = (int) drawerItem.getIdentifier();
        String title = ((Nameable) drawerItem).getName().toString();
        Fragment fragment = new AllQuotesFragment();
        //Drawer - fragments
        switch (id) {
            case R.string.menu_all_quotes:
                fragment = new AllQuotesFragment();
                break;
            //Just open the lists
            case R.string.menu_authors:
                return false;
            case R.string.menu_categories:
                return false;
            case R.string.search_quotes:
                fragment = new SearchQuotesFragment();
                if (!Storage.getPremium(this)) {
                    Snackbar.buyPremiumVersion(this, getSupportFragmentManager(), new PremiumFragment());
                    return false;
                }
                break;
            case R.string.menu_favorites:
                fragment = new FavoritesFragment();
                break;
            case R.string.menu_my_images:
                fragment = new MyImagesFragment();
                break;
            case R.string.menu_text_recognition:
                Snackbar.showText(this, R.string.loading);
                startActivity(new Intent(this, TextRecognitionActivity.class));
                return false;
            case R.string.menu_premium_version:
                if (Storage.getPremium(this)){
                    Snackbar.showText(this, R.string.you_already_have_premium_version);
                }
                fragment = new PremiumFragment();
                break;
            case R.string.menu_settings:
                fragment = new SettingsFragment();
                break;
            case R.string.menu_exit:
                finish();
                break;
            default:
                Bundle bundle = new Bundle();
                bundle.putString(Key.TITLE, title);
                bundle.putString(Key.QUERY, getResources().getString(id));
                //Set fragment arguments
                fragment = new QuotesFragment();
                fragment.setArguments(bundle);
                break;
        }
        Fragments.replaceWithHistory(getSupportFragmentManager(), fragment);
        return false;
    }
}