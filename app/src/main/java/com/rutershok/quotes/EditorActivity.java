package com.rutershok.quotes;


import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.tabs.TabLayout;
import com.rutershok.quotes.adapters.EditorPagerAdapter;
import com.rutershok.quotes.models.Editor;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Ad;
import com.rutershok.quotes.utils.Dialog;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Share;

import java.util.ArrayList;
import java.util.Random;

public class EditorActivity extends AppCompatActivity {

    private TextView textQuote;
    private ImageView imageBackground;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        Ad.showInterstitialWithFrequency(this);
        setActionBar();
        setQuoteImage();
        setEditor();

        Dialog.showRemoveLogo(this);
    }

    private void setEditor() {
        ViewPager viewPager = findViewById(R.id.view_pager_editor);
        viewPager.setAdapter(new EditorPagerAdapter(getSupportFragmentManager(), this));

        TabLayout tabLayout = findViewById(R.id.tab_layout_editor);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setActionBar() {
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.create_image));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    private void setRandomTemplate() {
        ArrayList<Typeface> fontList = Editor.getFonts(this);
        ArrayList<Drawable> imagesList = Editor.getFreeImages(this);
        textQuote.setTypeface(fontList.get(new Random().nextInt(fontList.size())));
        textQuote.setTextColor(ContextCompat.getColor(this, R.color.white));
        imageBackground.setImageDrawable(imagesList.get(new Random().nextInt(imagesList.size())));
    }

    private void setQuoteImage() {
        textQuote = findViewById(R.id.text_image_quote);
        imageBackground = findViewById(R.id.image_image_background);

        String quote = getIntent().getStringExtra(Key.QUOTE);
        String extraText = getIntent().getStringExtra(Intent.EXTRA_TEXT);

        if (extraText != null) {
            quote = extraText;
        }

        if (quote != null) {
            StringBuilder stringBuilder = new StringBuilder(quote);
            String author = getIntent().getStringExtra(Key.AUTHOR);

            if (author != null) {
                stringBuilder.append(author);
            }
            textQuote.setText(stringBuilder);
        }

        textQuote.setOnClickListener(v -> Dialog.textEditQuote(v.getContext(), textQuote));

        final ImageView imageLogo = findViewById(R.id.image_logo);

        //Remove logo from premium versions
        if (Storage.getPremium(this)) {
            imageLogo.setVisibility(View.GONE);
        } else {
            imageLogo.setOnClickListener(v -> Ad.showRemoveLogo(EditorActivity.this, imageLogo));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.action_save:
                Share.saveImage(this);
                break;
            case R.id.action_share:
                Ad.showInterstitial(this);
                new Dialog.ShareBottomSheet(this).show();
                break;
            case R.id.action_shuffle:
                setRandomTemplate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Glide.with(this).load(data.getData()).apply(new RequestOptions().centerCrop()).into(imageBackground);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}