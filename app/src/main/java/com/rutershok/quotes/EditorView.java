package com.rutershok.quotes;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.rutershok.quotes.R;

public class EditorView extends ConstraintLayout implements View.OnClickListener {
    private View.OnClickListener listener;

    public EditorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs);
    }

    private void setup(@NonNull Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.item_editor, this);
        ConstraintLayout layout = findViewById(R.id.cl_editor);
        layout.setOnClickListener(this);
        setStyleable(context, attrs);
    }

    private void setStyleable(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EditorView);
        try {
            int iconResId = typedArray.getResourceId(R.styleable.EditorView_editorIcon, 0);
            int titleResId = typedArray.getResourceId(R.styleable.EditorView_editorTitle, 0);

            Glide.with(context).load(iconResId).into(((ImageView) findViewById(R.id.image_editor_icon)));
            ((TextView) findViewById(R.id.text_editor_title)).setText(titleResId);
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            listener.onClick(view);
    }
}
