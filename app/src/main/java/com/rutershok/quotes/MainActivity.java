package com.rutershok.quotes;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.rutershok.quotes.fragments.AllQuotesFragment;
import com.rutershok.quotes.utils.Ad;
import com.rutershok.quotes.utils.Billing;
import com.rutershok.quotes.utils.Notifications;

public class MainActivity extends DrawerSupport {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new AllQuotesFragment()).commit();

        initializeDrawer();

        Ad.initialize(this);
        Ad.showBanner(this);

        Billing.initialize(this);
        Notifications.initialize(this);

        //Dialog.rateThisApp(this);
        ReviewManager manager = ReviewManagerFactory.create(this);
        Task<ReviewInfo> request = manager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We can get the ReviewInfo object
                ReviewInfo reviewInfo = task.getResult();
                manager.launchReviewFlow(MainActivity.this, reviewInfo);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
}