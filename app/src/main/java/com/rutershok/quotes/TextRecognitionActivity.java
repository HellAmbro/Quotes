package com.rutershok.quotes;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.rutershok.quotes.utils.Key;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;

public class TextRecognitionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_recognition);
        CropImage.activity().setBorderLineColor(Color.RED)
                .setAutoZoomEnabled(true)
                .setGuidelines(CropImageView.Guidelines.ON).start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri uri = result.getUri();
                if (uri == null) {
                    finish();
                } else {
                    recognizeText(uri);
                }
            } else {
                finish();
            }
        } else {
            finish();
        }
    }

    private void recognizeText(Uri uri) {
       /* try {
            FirebaseVision.getInstance().getOnDeviceTextRecognizer().processImage(FirebaseVisionImage.fromFilePath(this, uri))
                    .addOnSuccessListener(firebaseVisionText -> {
                        StringBuilder quote = new StringBuilder();
                        for (FirebaseVisionText.TextBlock block :firebaseVisionText.getTextBlocks()) {
                            quote.append(block.getText());
                        }

                        if (quote.toString().equals("")) {
                            Toast.makeText(TextRecognitionActivity.this, R.string.no_text_recognized, Toast.LENGTH_LONG).show();
                            TextRecognitionActivity.this.finish();
                        } else {
                            TextRecognitionActivity.this.startActivity(new Intent(TextRecognitionActivity.this, EditorActivity.class)
                                    .putExtra(Key.QUOTE, quote.toString())
                                    .putExtra(Key.AUTHOR, ""));
                            TextRecognitionActivity.this.finish();
                        }
                    })
                    .addOnFailureListener(
                            e -> {
                                Toast.makeText(TextRecognitionActivity.this, "Fail", Toast.LENGTH_LONG).show();
                                TextRecognitionActivity.this.finish();
                            });
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
