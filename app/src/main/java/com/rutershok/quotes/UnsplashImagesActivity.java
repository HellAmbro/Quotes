package com.rutershok.quotes;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kc.unsplash.Unsplash;
import com.kc.unsplash.api.Order;
import com.kc.unsplash.models.Photo;
import com.rutershok.quotes.adapters.UnsplashImagesAdapter;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class UnsplashImagesActivity extends AppCompatActivity {
    private final ArrayList<Photo> backgroundsList = new ArrayList<>();
    private int page = 1;
    private Unsplash mUnsplash;
    private UnsplashImagesAdapter backgroundsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backgrounds);

        setActionBar();
        setRecyclerView();
    }

    private void setActionBar() {
        setSupportActionBar(findViewById(R.id.toolbar));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.choose_background));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    private void setRecyclerView() {
        loadBackgrounds();

        backgroundsAdapter = new UnsplashImagesAdapter(this, backgroundsList);
        RecyclerView recyclerView = findViewById(R.id.recycler_backgrounds_backgrounds);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(backgroundsAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    loadBackgrounds();
                }
            }
        });
    }

    private void loadBackgrounds() {
        final ProgressBar progressBar = findViewById(R.id.pb_load_backgrounds);
        progressBar.setVisibility(View.VISIBLE);
        getUnsplash().getPhotos(page, 18, Order.LATEST, new Unsplash.OnPhotosLoadedListener() {
            @Override
            public void onComplete(List<Photo> photos) {
                progressBar.setVisibility(View.INVISIBLE);
                backgroundsList.addAll(photos);
                backgroundsAdapter.notifyDataSetChanged();
                ++page;
            }

            @Override
            public void onError(String error) {
                Snackbar.showText(UnsplashImagesActivity.this, R.string.error);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private Unsplash getUnsplash() {
        if (mUnsplash == null) {
            mUnsplash = new Unsplash(Key.UNSPLASH);
        }
        return mUnsplash;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
