package com.rutershok.quotes.adapters;


import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.R;
import com.rutershok.quotes.UnsplashImagesActivity;
import com.rutershok.quotes.models.Editor;
import com.rutershok.quotes.utils.Ad;
import com.rutershok.quotes.utils.Dialog;
import com.rutershok.quotes.utils.Key;
import com.theartofdev.edmodo.cropper.CropImage;

public class BackgroundEditorAdapter extends RecyclerView.Adapter<BackgroundEditorAdapter.ViewHolder> {

    private final Editor.Background[] editors;
    private final Activity mActivity;

    public BackgroundEditorAdapter(Activity activity) {
        this.editors = Editor.Background.values();
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public BackgroundEditorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BackgroundEditorAdapter.ViewHolder holder, int position) {
        final Editor.Background editor = editors[position];
        final ImageView imageBackground = mActivity.findViewById(R.id.image_image_background);
        holder.imageIcon.setImageResource(editor.iconResId);
        holder.textTitle.setText(editor.titleResId);
        holder.constraintLayoutEditor.setOnClickListener(v -> {
            switch (editor) {
                case BACKGROUND_COLOR:
                    Dialog.backgroundColor(mActivity, imageBackground);
                    break;
                case BACKGROUND_PHOTO:
                    CropImage.startPickImageActivity(mActivity);
                    break;
                case BACKGROUND_GRADIENT:
                    Dialog.backgroundGradient(mActivity, imageBackground);
                    break;
                case BACKGROUND_OPACITY:
                    Dialog.backgroundOpacity(mActivity, imageBackground);
                    break;
                case BACKGROUND_IMAGES:
                    backgroundImages();
                    break;
            }
        });
    }

    @Override
    public int getItemCount() {
        return editors.length;
    }

    private void backgroundImages() {
        Ad.showInterstitial(mActivity);
        mActivity.startActivityForResult(new Intent(mActivity, UnsplashImagesActivity.class), Key.GET_FROM_GALLERY);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final ConstraintLayout constraintLayoutEditor;
        final ImageView imageIcon;
        final TextView textTitle;

        ViewHolder(View view) {
            super(view);
            constraintLayoutEditor = view.findViewById(R.id.cl_editor);
            imageIcon = view.findViewById(R.id.image_editor_icon);
            textTitle = view.findViewById(R.id.text_editor_title);
        }
    }
}
