package com.rutershok.quotes.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.rutershok.quotes.fragments.EditBackgroundFragment;
import com.rutershok.quotes.fragments.EditTextFragment;
import com.rutershok.quotes.R;

public class EditorPagerAdapter extends FragmentPagerAdapter {

    private static final int ITEMS = 2;
    private final Context mActivity;

    public EditorPagerAdapter(FragmentManager fragmentManager, Context activity) {
        super(fragmentManager);
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        // Background
        if (position == 1) { // Quote
            return new EditTextFragment();
        }
        return new EditBackgroundFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mActivity.getString(R.string.background);
            case 1:
                return mActivity.getString(R.string.text);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return ITEMS;
    }
}
