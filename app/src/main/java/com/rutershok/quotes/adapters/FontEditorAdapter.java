package com.rutershok.quotes.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.models.Editor;
import com.rutershok.quotes.R;

import java.util.ArrayList;

public class FontEditorAdapter extends RecyclerView.Adapter<FontEditorAdapter.ViewHolder> {

    private final ArrayList<Typeface> fontsList;
    private final TextView textView;

    public FontEditorAdapter(Context context, TextView textView) {
        this.fontsList = Editor.getFonts(context);
        this.textView = textView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editor_font, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.textFont.setTypeface(fontsList.get(position));
        holder.itemView.setOnClickListener(v -> textView.setTypeface(fontsList.get(holder.getAdapterPosition())));
    }

    @Override
    public int getItemCount() {
        return fontsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final TextView textFont;

        ViewHolder(View view) {
            super(view);
            textFont = view.findViewById(R.id.text_font);
        }
    }
}
