package com.rutershok.quotes.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rutershok.quotes.models.Editor;
import com.rutershok.quotes.R;

import java.util.ArrayList;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private final ArrayList<Drawable> imagesList;
    private final ImageView imageView;

    public ImagesAdapter(Context context, ImageView imageView) {
        this.imagesList = Editor.getFreeImages(context);
        this.imageView = imageView;
    }

    @NonNull
    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editor_background, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ImagesAdapter.ViewHolder holder, int position) {
        final Context context = holder.itemView.getContext();
        Glide.with(context).load(imagesList.get(position)).into(holder.imageImage);
        holder.imageImage.setOnClickListener(view -> imageView.setImageDrawable(imagesList.get(holder.getAdapterPosition())));
    }


    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView imageImage;

        ViewHolder(View view) {
            super(view);
            imageImage = view.findViewById(R.id.image_image);
        }
    }
}
