package com.rutershok.quotes.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.rutershok.quotes.R;
import com.rutershok.quotes.utils.Dialog;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Share;
import com.rutershok.quotes.utils.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class MyImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<File> myImages = new ArrayList<>();

    public MyImagesAdapter(Context context) {
        File[] savedImages = new File(Key.PARENT_PATH, context.getString(R.string.app_name)).listFiles();

        if (savedImages != null) {
            this.myImages = new ArrayList<>(Arrays.asList(savedImages));
        }

        if (myImages.isEmpty()) {
            Dialog.myImagesIsEmpty(context);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final File image = myImages.get(position);
        final ImageHolder imageHolder = (ImageHolder) holder;
        Glide.with(holder.itemView.getContext()).load(image).into(imageHolder.imageBackground);
        imageHolder.imageDelete.setOnClickListener(view -> Snackbar.deleteImage(view, v -> {
            if (deleteImage(imageHolder)) {
                Snackbar.showText(view, R.string.image_deleted);
            }
        }));
        imageHolder.imageShare.setOnClickListener(view -> Share.withMyImage(view, image.getName()));
    }

    private boolean deleteImage(ImageHolder holder) {
        if (myImages.get(holder.getAdapterPosition()).delete()) {
            int position = holder.getAdapterPosition();
            myImages.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, myImages.size());
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return myImages.size();
    }

    static class ImageHolder extends RecyclerView.ViewHolder {
        final ImageView imageBackground;
        final ImageView imageDelete;
        final ImageView imageShare;

        ImageHolder(View view) {
            super(view);
            imageShare = view.findViewById(R.id.image_share);
            imageDelete = view.findViewById(R.id.image_delete);
            imageBackground = view.findViewById(R.id.image_template_background);
        }
    }
}
