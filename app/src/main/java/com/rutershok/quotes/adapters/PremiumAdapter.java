package com.rutershok.quotes.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rutershok.quotes.R;
import com.rutershok.quotes.models.Premium;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Billing;


public class PremiumAdapter extends RecyclerView.Adapter<PremiumAdapter.ViewHolder> {

    public PremiumAdapter() {
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_premium, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Context context = holder.itemView.getContext();
        final Premium premium = Premium.values()[position];
        Glide.with(context).load(premium.iconResId).into(holder.imageIcon);
        holder.textName.setText(context.getString(premium.nameResId));
        holder.textPrice.setText(Storage.getPremiumPrice(context, premium.nameId));
        holder.itemView.setOnClickListener(v -> Billing.purchasePremium((Activity) v.getContext(), premium.nameId));
    }

    @Override
    public int getItemCount() {
        return Premium.values().length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView imageIcon;
        final TextView textName;
        final TextView textPrice;

        ViewHolder(View view) {
            super(view);
            imageIcon = view.findViewById(R.id.image_premium_icon);
            textName = view.findViewById(R.id.text_premium_name);
            textPrice = view.findViewById(R.id.text_premium_price);
        }
    }
}