package com.rutershok.quotes.adapters;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.EditorActivity;
import com.rutershok.quotes.models.Category;
import com.rutershok.quotes.models.Quote;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Fragments;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Share;
import com.rutershok.quotes.utils.Snackbar;

import java.util.ArrayList;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.QuoteHolder> {

    private final ArrayList<Quote> quotesList;
    private Context mContext;

    public QuotesAdapter(ArrayList<Quote> quotesList) {
        this.quotesList = quotesList;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public QuoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QuoteHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quote, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final QuoteHolder holder, final int position) {
        mContext = holder.itemView.getContext();
        //Set texts of quote and author
        final Quote quote = quotesList.get(holder.getAdapterPosition());
        holder.textCategory.setText(Category.translate(mContext, quote.getCategory()));
        holder.textQuote.setText(quote.getQuote());
        holder.textAuthor.setText(String.format("- %s", quote.getAuthor()));

        if (Storage.getFavoriteQuotes(holder.itemView.getContext()).contains(quote)) {
            holder.imageFavorite.setImageResource(R.drawable.ic_favorite_on);
        }
        
        setListeners(holder, quote);
    }

    @Override
    public int getItemCount() {
        return quotesList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void setListeners(final QuoteHolder holder, final Quote quote) {
        holder.textAuthor.setOnClickListener(view -> {
            String author = quote.getAuthor();
            String query = "Author='" + author.replaceAll("'", "''") + "'";
            Fragments.replaceWithHistory(mContext, author, query);
        });

        holder.textCategory.setOnClickListener(view -> {
            String category = holder.textCategory.getText().toString();
            String query = "Category='" + Category.queryName(mContext, category).replaceAll("'", "''") + "'";
            Fragments.replaceWithHistory(mContext, category, query);
        });

        holder.imageFavorite.setOnClickListener(view -> setFavorite(holder, quote));

        holder.imageShare.setOnClickListener(view -> Share.withText(mContext, quote.getQuote()));

        holder.imageCopyToClipboard.setOnClickListener(view -> {
            Share.copyToClipboard(mContext, quote.getQuote());
            Snackbar.showText(view, R.string.copy_to_clipboard);
        });

        holder.cardViewQuote.setOnClickListener(v -> mContext.startActivity(new Intent(mContext, EditorActivity.class)
                .putExtra(Key.QUOTE, quote.getQuote())
                .putExtra(Key.AUTHOR, "\n - " + quote.getAuthor())));
    }

    private void setFavorite(QuoteHolder holder, Quote quote) {

        int favoriteImage = R.drawable.ic_favorite_off;
        if (Storage.getFavoriteQuotes(mContext).contains(quote)) {
            Storage.removeFavoriteQuote(mContext, quote);
        } else {
            Storage.addFavoriteQuote(mContext, quote);
            favoriteImage = R.drawable.ic_favorite_on;
        }
        holder.imageFavorite.setImageResource(favoriteImage);
    }

    static class QuoteHolder extends RecyclerView.ViewHolder {
        final CardView cardViewQuote;
        final TextView textCategory;
        final TextView textQuote;
        final TextView textAuthor;
        final ImageView imageCopyToClipboard;
        final ImageView imageFavorite;
        final ImageView imageShare;

        QuoteHolder(View view) {
            super(view);
            cardViewQuote = view.findViewById(R.id.card_quote);
            textCategory = view.findViewById(R.id.text_category);
            textQuote = view.findViewById(R.id.text_quote);
            textAuthor = view.findViewById(R.id.text_author);

            imageCopyToClipboard = view.findViewById(R.id.image_copy_to_clipboard);
            imageFavorite = view.findViewById(R.id.image_favorite);
            imageShare = view.findViewById(R.id.image_share);
        }
    }
}