package com.rutershok.quotes.adapters;


import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rutershok.quotes.models.Social;
import com.rutershok.quotes.R;
import com.rutershok.quotes.utils.Share;

public class SocialsAdapter extends RecyclerView.Adapter<SocialsAdapter.ViewHolder> {

    private final Activity mActivity;

    public SocialsAdapter(Activity activity) {
        this.mActivity = activity;
    }

    @NonNull
    @Override
    public SocialsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_social, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final SocialsAdapter.ViewHolder holder, final int position) {
        Glide.with(mActivity).load(Social.getAll().get(position).iconResId).into(holder.imageSocial);
        holder.cardSocial.setOnClickListener(v -> {
            Log.e("Social", "Clicked" + Social.getAll().get(holder.getAdapterPosition()));
            Share.withSocial(mActivity, Social.getAll().get(holder.getAdapterPosition()));
        });
    }

    @Override
    public int getItemCount() {
        return Social.getAll().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final CardView cardSocial;
        final ImageView imageSocial;

        ViewHolder(View view) {
            super(view);
            cardSocial = view.findViewById(R.id.card_social);
            imageSocial = view.findViewById(R.id.image_social);
        }
    }
}
