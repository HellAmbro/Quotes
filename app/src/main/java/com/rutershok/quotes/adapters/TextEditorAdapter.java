package com.rutershok.quotes.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.models.Editor;
import com.rutershok.quotes.R;
import com.rutershok.quotes.utils.Dialog;

public class TextEditorAdapter extends RecyclerView.Adapter<TextEditorAdapter.ViewHolder> {

    private final Editor.Text[] editors;
    private final Activity mActivity;
    private final TextView mTextView;

    public TextEditorAdapter(Activity activity) {
        this.editors = Editor.Text.values();
        this.mActivity = activity;
        this.mTextView = mActivity.findViewById(R.id.text_image_quote);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Editor.Text editor = editors[position];
        final TextView mTextView = mActivity.findViewById(R.id.text_image_quote);
        holder.imageIcon.setImageResource(editor.iconResId);
        holder.textTitle.setText(editor.titleResId);
        holder.constraintLayoutEditor.setOnClickListener(v -> {
            switch (editor) {
                case TEXT_COLOR:
                    Dialog.textColor(mActivity, mTextView);
                    break;
                case TEXT_SIZE:
                    Dialog.textSize(mActivity, mTextView);
                    break;
                case TEXT_ALIGNMENT:
                    textAlignment();
                    break;
                case TEXT_OPACITY:
                    Dialog.textOpacity(mActivity, mTextView);
                    break;
                case TEXT_SPACING:
                    Dialog.textSpacing(mActivity, mTextView);
                    break;
                case TEXT_HIGHLIGHT:
                    Dialog.textHighlight(mActivity, mTextView);
                    break;
                case TEXT_BORDER:
                    Dialog.textBorder(mActivity, mTextView);
                    break;
            }
        });
    }

    @Override
    public int getItemCount() {
        return editors.length;
    }

    private void textAlignment() {
        switch (mTextView.getGravity()) {
            case Gravity.END:
            case 8388661:
                mTextView.setGravity(Gravity.START);
                break;
            case Gravity.CENTER:
                mTextView.setGravity(Gravity.END);
                break;
            case Gravity.START:
            case 8388659:
                mTextView.setGravity(Gravity.CENTER);
                break;
            default:
                Log.e("gravity", mTextView.getGravity() + "");
                break;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ConstraintLayout constraintLayoutEditor;
        final ImageView imageIcon;
        final TextView textTitle;

        ViewHolder(View view) {
            super(view);
            constraintLayoutEditor = view.findViewById(R.id.cl_editor);
            imageIcon = view.findViewById(R.id.image_editor_icon);
            textTitle = view.findViewById(R.id.text_editor_title);
        }
    }
}
