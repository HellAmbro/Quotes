package com.rutershok.quotes.adapters;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kc.unsplash.models.Photo;
import com.rutershok.quotes.R;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;

public class UnsplashImagesAdapter extends RecyclerView.Adapter<UnsplashImagesAdapter.ViewHolder> {

    private final ArrayList<Photo> mBackgroundsList;
    private final Activity mActivity;

    public UnsplashImagesAdapter(Activity activity, ArrayList<Photo> backgroundsList) {
        mBackgroundsList = backgroundsList;
        mActivity = activity;
    }

    @NonNull
    @Override
    public UnsplashImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UnsplashImagesAdapter.ViewHolder holder, int position) {
        final Uri uri = Uri.parse(mBackgroundsList.get(position).getUrls().getRegular());
        Glide.with(holder.itemView.getContext())
                .setDefaultRequestOptions(new RequestOptions().centerCrop().placeholder(R.drawable.ic_my_images))
                .load(uri)
                .into(holder.imageBackground);
        //holder.textAuthor.setText(String.format("By %s on Unsplash", mBackgroundsList.get(position).getUser().getName()));
        holder.itemView.setOnClickListener(v -> {
            Intent data = new Intent();
            data.setData(uri);
            mActivity.setResult(CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE, data);
            mActivity.finish();
        });
    }

    @Override
    public int getItemCount() {
        return mBackgroundsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView imageBackground;
        //final TextView textAuthor;
        ViewHolder(View view) {
            super(view);
            imageBackground = view.findViewById(R.id.image_image_unsplash);
            //textAuthor = view.findViewById(R.id.text_background_author);
        }
    }
}
