package com.rutershok.quotes.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.rutershok.quotes.EditorActivity;
import com.rutershok.quotes.R;
import com.rutershok.quotes.adapters.QuotesAdapter;
import com.rutershok.quotes.models.Quote;
import com.rutershok.quotes.storage.Database;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Ad;
import com.rutershok.quotes.utils.Dialog;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllQuotesFragment extends Fragment {

    private static final ArrayList<Quote> mQuotesList = new ArrayList<>();
    private View mView;
    private Activity mActivity;
    private QuotesAdapter mQuotesAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quotes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        if (getActivity() != null) {
            mView = view;
            mActivity = getActivity();
            getActivity().setTitle(R.string.menu_all_quotes);
            setRecyclerView();
            setSwipeRefresh();
            Ad.showInterstitialWithFrequency(mActivity);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_all_quotes, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_today_quote:
                Dialog.quoteOfTheDay(mActivity);
                break;
            case R.id.action_create:
                mActivity.startActivity(new Intent(mActivity, EditorActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSwipeRefresh() {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_quotes);
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mSwipeRefreshLayout.setRefreshing(true);
            loadAllQuotes();
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    private void setRecyclerView() {
        mQuotesAdapter = new QuotesAdapter(mQuotesList);

        if (mQuotesList.size() == 0) {
            loadAllQuotes();
        }

        RecyclerView recyclerView = mView.findViewById(R.id.recycler_quotes);
        recyclerView.setAdapter(mQuotesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    loadAllQuotes();
                    Ad.showInterstitialWithFrequency(mActivity);
                }
            }
        });
    }

    private void loadAllQuotes() {
        final ProgressBar progressBar = mView.findViewById(R.id.pb_load_quotes);
        progressBar.setVisibility(View.VISIBLE);
        String url = Key.API_URL + "get_all_quotes.php"
                + "?from=" + mQuotesList.size()
                + "&limit=" + Key.LIMIT
                + "&lang=" + Storage.getLanguage(mActivity);
        Log.e("url", url);
        Database.getRequestQueue(mActivity).add(new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONArray jsonArray = new JSONObject(response).getJSONArray(Key.QUOTES);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            Quote quote = new Quote();
                            quote.setQuote(object.getString(Key.QUOTE));
                            quote.setAuthor(object.getString(Key.AUTHOR));
                            quote.setCategory(object.getString(Key.CATEGORY));
                            if (!mQuotesList.contains(quote)) {
                                mQuotesList.add(quote);
                                mQuotesAdapter.notifyDataSetChanged();
                            }
                        }
                        if (jsonArray.length() == 0) {
                            progressBar.setVisibility(View.GONE);
                            Snackbar.showText(mView, R.string.quotes_finished);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);
                    }
                }, error -> {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.networkUnavailable(getActivity(), v -> loadAllQuotes());
                }));
    }
}
