package com.rutershok.quotes.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.adapters.BackgroundEditorAdapter;
import com.rutershok.quotes.adapters.ImagesAdapter;
import com.rutershok.quotes.R;

public class EditBackgroundFragment extends Fragment {

    private Activity mActivity;
    private ImageView mImageView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return getLayoutInflater().inflate(R.layout.fragment_edit_background, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            mActivity = getActivity();
            mImageView = mActivity.findViewById(R.id.image_image_background);
        }
        setRecyclerEditorItems(view);
        setRecyclerImages(view);
    }

    private void setRecyclerEditorItems(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_background_editors);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new BackgroundEditorAdapter(mActivity));
    }

    private void setRecyclerImages(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_images);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new ImagesAdapter(view.getContext(), mImageView));
    }
}
