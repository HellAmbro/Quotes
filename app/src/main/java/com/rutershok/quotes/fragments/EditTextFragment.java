package com.rutershok.quotes.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.adapters.FontEditorAdapter;
import com.rutershok.quotes.adapters.TextEditorAdapter;
import com.rutershok.quotes.R;

public class EditTextFragment extends Fragment {

    private Activity mActivity;
    private TextView mTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.fragment_edit_text, null);
        setRetainInstance(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            mActivity = getActivity();
            mTextView = getActivity().findViewById(R.id.text_image_quote);
        }
        setRecyclerEditorItems(view);
        setRecyclerFonts(view);
    }

    private void setRecyclerEditorItems(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_text_editors);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new TextEditorAdapter(mActivity));
    }

    private void setRecyclerFonts(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_fonts);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new FontEditorAdapter(view.getContext(), mTextView));
    }
}
