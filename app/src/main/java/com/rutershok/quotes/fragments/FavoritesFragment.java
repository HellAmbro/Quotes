package com.rutershok.quotes.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.adapters.QuotesAdapter;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Ad;
import com.rutershok.quotes.utils.Dialog;

public class FavoritesFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quotes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
        if (getActivity() != null) {
            getActivity().setTitle(R.string.menu_favorites);
            setRecyclerView(view);
            Ad.showInterstitialWithFrequency(getActivity());
        }
    }

    private void setRecyclerView(View view) {
        //Set RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_quotes);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        if (Storage.getFavoriteQuotes(view.getContext()).isEmpty()) {
            Dialog.noFavoriteQuotes(view.getContext());
        } else {
            recyclerView.setAdapter(new QuotesAdapter(Storage.getFavoriteQuotes(view.getContext())));
        }
    }
}