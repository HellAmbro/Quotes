package com.rutershok.quotes.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rutershok.quotes.adapters.PremiumAdapter;
import com.rutershok.quotes.R;
import com.rutershok.quotes.utils.Ad;

public class PremiumFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_premium_version, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
        if (getActivity() != null) {
            getActivity().setTitle(R.string.menu_premium_version);
            setRecyclerView(view);
            Ad.showInterstitialWithFrequency(getActivity());
        }
    }

    private void setRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_buy_items);
        recyclerView.setAdapter(new PremiumAdapter());
        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
    }
}