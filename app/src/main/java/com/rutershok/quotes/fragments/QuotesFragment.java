package com.rutershok.quotes.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.rutershok.quotes.adapters.QuotesAdapter;
import com.rutershok.quotes.models.Quote;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Database;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Ad;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class QuotesFragment extends Fragment {

    private final ArrayList<Quote> quotesList = new ArrayList<>();
    private Activity mActivity;
    private View mView;
    private String query;
    private QuotesAdapter quotesAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_quotes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
        //setHasOptionsMenu(true);
        if (getActivity() != null && getArguments() != null) {
            mActivity = getActivity();
            mView = view;
            getActivity().setTitle(getArguments().getString(Key.TITLE));
            query = getArguments().getString(Key.QUERY);
            setRecyclerView();
            setSwipeRefresh();
            Ad.showInterstitialWithFrequency(mActivity);
        }
    }

    private void setSwipeRefresh() {
        // SwipeRefreshLayout
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_quotes);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(true);
            loadQuotes();
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }
    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    private void setRecyclerView() {
        loadQuotes();

        quotesAdapter = new QuotesAdapter(quotesList);
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_quotes);
        recyclerView.setAdapter(quotesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    loadQuotes();
                    Ad.showInterstitialWithFrequency(mActivity);
                }
            }
        });
    }

    private void loadQuotes() {
        final ProgressBar progressBar = mView.findViewById(R.id.pb_load_quotes);
        progressBar.setVisibility(View.VISIBLE);

        String url = null;
        try {
            url = Key.API_URL + "get_quotes.php"
                    + "?query=" + URLEncoder.encode(query, "UTF-8")
                    + "&from=" + quotesList.size()
                    + "&limit=" + Key.LIMIT
                    + "&lang=" + Storage.getLanguage(mActivity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("url", url);
        Database.getRequestQueue(mActivity).add(new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONArray jsonArray = new JSONObject(response).getJSONArray(Key.QUOTES);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            Quote quote = new Quote();
                            quote.setQuote(object.getString(Key.QUOTE));
                            quote.setAuthor(object.getString(Key.AUTHOR));
                            quote.setCategory(object.getString(Key.CATEGORY));
                            quotesList.add(quote);
                            quotesAdapter.notifyDataSetChanged();
                        }

                        if (quotesList.size() != 0 && jsonArray.length() == 0) {
                            progressBar.setVisibility(View.GONE);
                            Snackbar.showText(mView, R.string.quotes_finished);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        Log.e("Finally", quotesList.size() + "");
                    }
                }, error -> {
                    progressBar.setVisibility(View.GONE);
                    new Handler().postDelayed(() -> Snackbar.networkUnavailable(getActivity(), v -> loadQuotes()), 400);
                }));
    }
}