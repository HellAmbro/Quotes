package com.rutershok.quotes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.rutershok.quotes.adapters.QuotesAdapter;
import com.rutershok.quotes.models.Category;
import com.rutershok.quotes.models.Quote;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Database;
import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;


public class SearchQuotesFragment extends Fragment {

    private final ArrayList<Quote> mQuotesList = new ArrayList<>();
    private Context mContext;
    private View mView;
    private String category = "1"; //Is is default number that indicate whole database
    private String query;
    private final QuotesAdapter mQuotesAdapter =new QuotesAdapter(mQuotesList);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_quotes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        mContext = view.getContext();
        mView = view;
        if (getActivity() != null) {
            getActivity().setTitle(getString(R.string.search_quotes));
            setRecyclerView(view);
            setSpinner(view);
        }
    }

    private void setSpinner(View view) {
        Spinner spinner = view.findViewById(R.id.spinner_search_category);
        ArrayList<String> categoriesList = new ArrayList<>();

        categoriesList.add(getString(R.string.all_categories));
        categoriesList.addAll(Category.getNames(view.getContext()));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_dropdown_item, categoriesList);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    category = "1";
                } else {
                    if (view != null)
                        category = mContext.getString(Category.values()[position - 1].queryResId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setRecyclerView(final View view) {

        RecyclerView recyclerView = view.findViewById(R.id.recycler_search_quotes);
        recyclerView.setAdapter(mQuotesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    searchQuotes();
                }
                //-1 for top scrolling
                // Log.e("Position", layoutManager.findFirstVisibleItemPosition() + "");
            }
        });
    }

    private void searchQuotes() {
        String url = " ";
        final ProgressBar progressBar = mView.findViewById(R.id.pb_load_quotes);
        progressBar.setVisibility(View.VISIBLE);

        final TextView textNoResultsFound = mView.findViewById(R.id.text_no_results_found);
        textNoResultsFound.setVisibility(View.GONE);
        try {
            url = Key.API_URL + "search_quotes.php"
                    + "?search=" + URLEncoder.encode(query, "UTF-8")
                    + "&category=" + URLEncoder.encode(category, "UTF-8")
                    + "&from=" + mQuotesList.size()
                    + "&limit=" + Key.LIMIT
                    + "&lang=" + Storage.getLanguage(mContext);
            Log.e("url", url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Database.getRequestQueue(mContext).add(new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONArray jsonArray = new JSONObject(response).getJSONArray(Key.QUOTES);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            Quote quote = new Quote();
                            quote.setQuote(object.getString(Key.QUOTE));
                            quote.setAuthor(object.getString(Key.AUTHOR));
                            quote.setCategory(object.getString(Key.CATEGORY));
                            mQuotesList.add(quote);
                            mQuotesAdapter.notifyDataSetChanged();
                        }

                        if (mQuotesList.size() != 0 && jsonArray.length() == 0) {
                            progressBar.setVisibility(View.GONE);
                            Snackbar.showText(mView, R.string.quotes_finished);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        progressBar.setVisibility(View.GONE);
                        if (mQuotesList.isEmpty()) {
                            textNoResultsFound.setVisibility(View.VISIBLE);
                        }
                    }
                }, error -> {
                    progressBar.setVisibility(View.GONE);
                    new Handler().postDelayed(() -> Snackbar.networkUnavailable(getActivity(), v -> searchQuotes()), 400);
                }));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchQuotesFragment.this.query = query.toLowerCase();
                //Clear previous search
                mQuotesList.clear();
                searchQuotes();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
