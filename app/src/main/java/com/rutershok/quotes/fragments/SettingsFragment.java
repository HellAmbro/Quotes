package com.rutershok.quotes.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.rutershok.quotes.R;
import com.rutershok.quotes.utils.Dialog;
import com.rutershok.quotes.utils.Key;
import com.rutershok.quotes.utils.Setting;

public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }

    public void onViewCreated(@NonNull final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);

        if (getActivity() != null) {
            getActivity().setTitle(R.string.menu_settings);
        }

        for (String key : Key.PREFERENCES) {
            if (findPreference(key) != null) {
                findPreference(key).setOnPreferenceClickListener(this);
            }
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        Context context = preference.getContext();
        switch (preference.getKey()) {
            case Key.PREF_NOTIFICATIONS:
                Setting.setNotifications(preference);
                break;
            case Key.PREF_QUOTE_OF_THE_DAY_TIME:
                Setting.setTimePicker(context);
                break;
            case Key.PREF_INVITE:
                if (getActivity() != null) {
                    Setting.inviteFriends(getActivity());
                }
                break;
            case Key.PREF_RATE_APP:
                Setting.openAppPage(context);
                break;
            case Key.PREF_SHARE_APP:
                Setting.shareApp(context);
                break;
            case Key.PREF_GOOGLE_PLAY:
                Setting.openPublisherPage(context);
                break;
            case Key.PREF_INSTAGRAM:
                Setting.openInstagram(context);
                break;
            case Key.PREF_FACEBOOK:
                Setting.openFacebook(context);
                break;
            case Key.PREF_TWITTER:
                Setting.openTwitter(context);
                break;
            case Key.PREF_PRIVACY_POLICY:
                Setting.openPrivacyPolicy(context);
                break;
            case Key.PREF_GDPR:
                Dialog.gdprConsent(getActivity());
                break;
            case Key.PREF_CONTACT_US:
                Setting.contactUs(context);
                break;
        }
        return true;
    }
}