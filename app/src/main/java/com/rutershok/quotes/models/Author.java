package com.rutershok.quotes.models;

import com.rutershok.quotes.R;

public enum Author {
    //This name MUST BE EQUAL TO DATABASE NAME //BE CAREFUL!!
    ALBERT_EINSTEIN(R.string.aut_albert_einstein, R.string.query_albert_einstein),
    ARTHUR_SCHOPENHAUER(R.string.aut_arthur_schopenhauer, R.string.query_arthur_schopenhauer),
    CHARLES_BUKOWSKI(R.string.aut_charles_bukowski, R.string.query_charles_bukowski),
    DALAI_LAMA(R.string.aut_dalai_lama, R.string.query_dalai_lama),
    FRIEDRICH_NIETZSCHE(R.string.aut_friedrich_nietzsche, R.string.query_friedrich_nietzsche),
    JIM_MORRISON(R.string.aut_jim_morrison, R.string.query_jim_morrison),
    KHALIL_GIBRAN(R.string.aut_khalil_gibran, R.string.query_khalil_gibran),
    LEV_TOLSTOJ(R.string.aut_lev_tolstoj, R.string.query_lev_tolstoj),
    MAHATMA_GANDHI(R.string.aut_mahatma_gandhi, R.string.query_mahatma_gandhi),
    MARK_TWAIN(R.string.aut_mark_twain, R.string.query_mark_twain),
    NELSON_MANDELA(R.string.aut_nelson_mandela, R.string.query_nelson_mandela),
    OSCAR_WILDE(R.string.aut_oscar_wilde, R.string.query_oscar_wilde),
    PAULO_COELHO(R.string.aut_paulo_coelho, R.string.query_paulo_coelho),
    STEVE_JOBS(R.string.aut_steve_jobs, R.string.query_steve_jobs),
    WILLIAM_SHAKESPEARE(R.string.aut_william_shakespeare, R.string.query_william_shakespeare),;

    public final int nameResId;
    public final int queryResId;

    Author(int nameResId, int queryResId) {
        this.nameResId = nameResId;
        this.queryResId = queryResId;
    }
}
