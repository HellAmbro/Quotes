package com.rutershok.quotes.models;


import android.content.Context;

import com.rutershok.quotes.R;

import java.util.ArrayList;

public enum Category {

    LOVE(R.string.cat_key_love, R.string.cat_love, R.string.query_love),
    LIFE(R.string.cat_key_life, R.string.cat_life, R.string.query_life),
    BEHAVIOR(R.string.cat_key_behavior, R.string.cat_behavior, R.string.query_behavior),

    DESIRE(R.string.cat_key_desire, R.string.cat_desire, R.string.query_desire),
    DESTINY(R.string.cat_key_destiny, R.string.cat_destiny, R.string.query_destiny),
    DREAM(R.string.cat_key_dream, R.string.cat_dream, R.string.query_dream),
    EMOTIONS(R.string.cat_key_emotions, R.string.cat_emotions, R.string.query_emotions),
    EYES(R.string.cat_key_eyes, R.string.cat_eyes, R.string.query_eyes),
    FAMILY(R.string.cat_key_family, R.string.cat_family, R.string.query_family),
    FAMOUS_PEOPLE(R.string.cat_key_famous_people, R.string.cat_famous_people, R.string.query_famous_people),
    FAMOUS_PHRASES(R.string.cat_key_famous_phrases, R.string.cat_famous_phrases, R.string.query_famous_phrases),
    FEAR_AND_COURAGE(R.string.cat_key_fear_and_courage, R.string.cat_fear_and_courage, R.string.query_fear_and_courage),
    FREEDOM(R.string.cat_key_freedom, R.string.cat_freedom, R.string.query_freedom),
    FRIENDSHIP(R.string.cat_key_friendship, R.string.cat_friendship, R.string.query_friendship),
    HUG(R.string.cat_key_hug, R.string.cat_hug, R.string.query_hug),
    KISS(R.string.cat_key_kiss, R.string.cat_kiss, R.string.query_kiss),
    MODERN_TIMES(R.string.cat_key_modern_times, R.string.cat_modern_times, R.string.query_modern_times),
    MOODS(R.string.cat_key_moods, R.string.cat_moods, R.string.query_moods),
    MOTIVATIONAL(R.string.cat_key_motivational, R.string.cat_motivational, R.string.query_motivational),
    MUSIC(R.string.cat_key_music, R.string.cat_music, R.string.query_music),
    NATURE(R.string.cat_key_nature, R.string.cat_nature, R.string.query_nature),
    PHILOSOPHY(R.string.cat_key_philosophy, R.string.cat_philosophy, R.string.query_philosophy),
    POETRY(R.string.cat_key_poetry, R.string.cat_poetry, R.string.query_poetry),
    SCIENCE_AND_TECHNOLOGY(R.string.cat_key_science_and_technology, R.string.cat_science_and_technology, R.string.query_science_and_technology),
    SILENCE(R.string.cat_key_silence, R.string.cat_silence, R.string.query_silence),
    SMILE(R.string.cat_key_smile, R.string.cat_smile, R.string.query_smile),
    SOUL(R.string.cat_key_soul, R.string.cat_soul, R.string.query_soul),
    SPORT(R.string.cat_key_sport, R.string.cat_sport, R.string.query_sport),
    SUCCESS(R.string.cat_key_success, R.string.cat_success, R.string.query_success),
    TIME(R.string.cat_key_time, R.string.cat_time, R.string.query_time),
    WISDOM(R.string.cat_key_wisdom, R.string.cat_wisdom, R.string.query_wisdom),
    WORK(R.string.cat_key_work, R.string.cat_work, R.string.query_work),;

    public final int nameResId;
    public final int queryResId;
    private final int keyResId;

    Category(int keyResId, int nameResId, int queryResId) {
        this.keyResId = keyResId;
        this.nameResId = nameResId;
        this.queryResId = queryResId;
    }

    public static String translate(Context context, String categoryName) {
        for (Category category : Category.values()) {
            if (categoryName.equals(context.getString(category.keyResId))) {
                return context.getString(category.nameResId);
            }
        }
        return context.getString(R.string.cat_love);
    }

    public static String queryName(Context context, String categoryName) {
        for (Category category : Category.values()) {
            if (categoryName.equals(context.getString(category.nameResId))) {
                return context.getString(category.keyResId);
            }
        }
        return context.getString(LOVE.keyResId);
    }

    public static ArrayList<String> getNames(Context context) {
        ArrayList<String> list = new ArrayList<>();
        for (Category category : Category.values()) {
            list.add(context.getString(category.nameResId));
        }
        return list;
    }
}
