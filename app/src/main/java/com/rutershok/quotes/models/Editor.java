package com.rutershok.quotes.models;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Editor {
    private static final ArrayList<Typeface> fontsList = new ArrayList<>();
    private static final ArrayList<Drawable>
            imagesList = new ArrayList<>();

    public static ArrayList<Typeface> getFonts(Context context) {
        AssetManager assetManager = context.getAssets();
        if (fontsList.isEmpty()) {

            //Default Fonts
            fontsList.add(Typeface.SANS_SERIF);
            fontsList.add(Typeface.SERIF);
            fontsList.add(Typeface.MONOSPACE);

            try {
                String[] namesList = assetManager.list("fonts");
                if (namesList != null) {
                    for (String name : namesList) {
                        //Avoid material drawer font to be added to custom fonts
                        if (name.contains("free")) {
                            fontsList.add(Typeface.createFromAsset(assetManager, "fonts/" + name));
                        }

                        if (name.contains("premium") && Storage.getPremium(context)) {
                            fontsList.add(Typeface.createFromAsset(assetManager, "fonts/" + name));
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fontsList;
    }

    public static ArrayList<Drawable> getFreeImages(Context context) {
        AssetManager assetManager = context.getAssets();
        if (imagesList.isEmpty()) {
            try {
                String[] namesList = assetManager.list("images");
                if (namesList != null) {
                    for (String name : namesList) {
                        if (name.contains("img")) {
                            try {
                                InputStream inputStream = assetManager.open("images/" + name);
                                imagesList.add(Drawable.createFromStream(inputStream, null));
                                inputStream.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imagesList;
    }

    public enum Background {
        BACKGROUND_COLOR(R.string.color, R.drawable.ic_color),
        BACKGROUND_PHOTO(R.string.photo, R.drawable.ic_add_a_photo),
        BACKGROUND_GRADIENT(R.string.gradient, R.drawable.ic_gradient),
        BACKGROUND_OPACITY(R.string.opacity, R.drawable.ic_opacity),
        BACKGROUND_IMAGES(R.string.images, R.drawable.ic_images);
        public final int titleResId;
        public final int iconResId;

        Background(int titleResId, int iconResId) {
            this.titleResId = titleResId;
            this.iconResId = iconResId;
        }
    }

    public enum Text {
        TEXT_COLOR(R.string.color, R.drawable.ic_color),
        TEXT_SIZE(R.string.text_size, R.drawable.ic_text_size),
        TEXT_OPACITY(R.string.opacity, R.drawable.ic_opacity),
        TEXT_ALIGNMENT(R.string.alignment, R.drawable.ic_alignment),
        TEXT_SPACING(R.string.spacing, R.drawable.ic_spacing),
        TEXT_HIGHLIGHT(R.string.highlight, R.drawable.ic_highlight),
        TEXT_BORDER(R.string.border, R.drawable.ic_border);

        public final int titleResId;
        public final int iconResId;

        Text(int titleResId, int iconResId) {
            this.titleResId = titleResId;
            this.iconResId = iconResId;
        }
    }

}
