package com.rutershok.quotes.models;

import com.rutershok.quotes.R;
import com.rutershok.quotes.utils.Key;

public enum Premium {
    ICE_CREAM(Key.PREMIUM_ICE_CREAM, R.drawable.ic_meal_ice_cream, R.string.meal_ice_cream),
    COFFEE(Key.PREMIUM_CHIPS, R.drawable.ic_meal_chips, R.string.meal_chips),
    HAMBURGER(Key.PREMIUM_BURGER, R.drawable.ic_meal_burger, R.string.meal_burger),
    PIZZA(Key.PREMIUM_PIZZA, R.drawable.ic_meal_pizza, R.string.meal_pizza);

    public final String nameId;
    public final int iconResId;
    public final int nameResId;

    Premium(String nameId, int iconResId, int nameResId) {
        this.nameId = nameId;
        this.iconResId = iconResId;
        this.nameResId = nameResId;
    }
}