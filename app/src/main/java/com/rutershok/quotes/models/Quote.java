package com.rutershok.quotes.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Quote {

    @SerializedName("Quote")
    private String quote;
    @SerializedName("Author")
    private String author;
    @SerializedName("Category")
    private String category;

    public Quote() {
    }

    @NonNull
    @Override
    public String toString() {
        return (quote + " " + author + " " + category);
    }

    //Used for favorites
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Quote)) {
            return false;
        }
        Quote quote = (Quote) object;
        return this.quote.equals(quote.quote);
    }

    @Override
    public int hashCode() {
        return quote.hashCode();
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
