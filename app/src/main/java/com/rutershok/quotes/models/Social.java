package com.rutershok.quotes.models;


import com.rutershok.quotes.R;

import java.util.ArrayList;
import java.util.Arrays;

public enum Social {
    WHATSAPP("com.whatsapp", R.drawable.ic_whatsapp_share),
    INSTAGRAM("com.instagram.android", R.drawable.ic_instagram_share),
    FACEBOOK("com.facebook.katana", R.drawable.ic_facebook_share),
    MESSENGER("com.facebook.orca", R.drawable.ic_messenger_share),
    TWITTER("com.twitter.android", R.drawable.ic_twitter_share),
    SNAPCHAT("com.snapchat.android", R.drawable.ic_snapchat_share),
    TUMBRL("com.tumblr", R.drawable.ic_tumblr_share),
    LINE("jp.naver.line.android", R.drawable.ic_line),
    HANGOUTS("com.google.android.talk", R.drawable.ic_hangouts),
    WECHAT("com.tencent.mm", R.drawable.ic_wechat);
    private static final ArrayList<Social> all = new ArrayList<>();
    public final String packageName;
    public final int iconResId;

    Social(String packageName, int iconResId) {
        this.packageName = packageName;
        this.iconResId = iconResId;
    }

    public static ArrayList<Social> getAll() {
        if (all.isEmpty()) {
            all.addAll(Arrays.asList(Social.values()));
        }
        return all;
    }
}
