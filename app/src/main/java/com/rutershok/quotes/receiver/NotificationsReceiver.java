package com.rutershok.quotes.receiver;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.rutershok.quotes.storage.Storage;
import com.rutershok.quotes.utils.Notifications;

import java.util.Calendar;

public class NotificationsReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(final Context context, Intent intent) {
        if (Storage.getNotifications(context) && Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == Storage.getQuoteOfTheDayHour(context)) {
            Notifications.send(context);
        }
    }
}