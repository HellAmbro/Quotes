package com.rutershok.quotes.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rutershok.quotes.models.Quote;
import com.rutershok.quotes.utils.Key;

import java.util.ArrayList;
import java.util.Locale;

public class Storage {

    private static SharedPreferences mPreferences;

    private static SharedPreferences getPreferences(Context context) {
        if (mPreferences == null) {
            mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return mPreferences;
    }

    public static void addFavoriteQuote(Context context, Quote quote) {
        ArrayList<Quote> favoriteQuotes = getFavoriteQuotes(context);
        if (!favoriteQuotes.contains(quote)) {
            favoriteQuotes.add(quote);
            setFavoriteQuotes(context, favoriteQuotes);
        }
    }

    public static void removeFavoriteQuote(Context context, Quote quote) {
        ArrayList<Quote> favoriteQuotes = getFavoriteQuotes(context);
        favoriteQuotes.remove(quote);
        setFavoriteQuotes(context, favoriteQuotes);
    }

    private static void setFavoriteQuotes(Context context, ArrayList<Quote> favoriteQuotes) {
        try {
            getPreferences(context).edit().putString(Key.FAVORITE_QUOTES, new Gson().toJson(favoriteQuotes)).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Quote> getFavoriteQuotes(Context context) {
        ArrayList<Quote> favoriteQuotes = new Gson().fromJson(getPreferences(context).getString(Key.FAVORITE_QUOTES, null), //Get string
                new TypeToken<ArrayList<Quote>>() {
                }.getType());
        return favoriteQuotes == null ? new ArrayList<>() : favoriteQuotes;
    }

    public static void setPremiumPrice(Context context, @NonNull String nameId, @NonNull String price) {
        getPreferences(context).edit().putString(nameId, price).apply();
    }

    public static String getPremiumPrice(Context context, @NonNull String nameId) {
        return getPreferences(context).getString(nameId, "0");
    }

    public static void setPremium(Context context, boolean isPremium) {
        getPreferences(context).edit().putBoolean(Key.PREMIUM, isPremium).apply();
    }

    public static boolean getPremium(Context context) {
        return getPreferences(context).getBoolean(Key.PREMIUM, false);
    }

    public static boolean getNotifications(Context context) {
        return getPreferences(context).getBoolean(Key.PREF_NOTIFICATIONS, true);
    }

    public static void setNotifications(Context context, boolean isEnabled) {
        getPreferences(context).edit().putBoolean(Key.PREF_NOTIFICATIONS, isEnabled).apply();
    }

    public static void setQuoteOfTheDayHour(Context context, int hour) {
        getPreferences(context).edit().putInt(Key.HOUR, hour).apply();
    }

    public static int getQuoteOfTheDayHour(Context context) {
        return getPreferences(context).getInt(Key.HOUR, 8);
    }

    public static void setQuoteOfTheDayMinute(Context context, int minute) {
        getPreferences(context).edit().putInt(Key.MINUTE, minute).apply();
    }

    public static int getQuoteOfTheDayMinute(Context context) {
        return getPreferences(context).getInt(Key.MINUTE, 0);
    }

    public static void updateInterstitialCount(Context context) {
        getPreferences(context).edit().putInt(Key.INTERSTITIAL_COUNT, getInterstitialCount(context) + 1).apply();
    }

    public static int getInterstitialCount(Context context) {
        return getPreferences(context).getInt(Key.INTERSTITIAL_COUNT, 1);
    }

    public static String getLanguage(Context context) {
        return getPreferences(context).getString(Key.LANGUAGE, Locale.getDefault().getLanguage());
    }

    public static void setRemoveLogoShowed(Context context, boolean isShowed) {
        getPreferences(context).edit().putBoolean(Key.REMOVE_LOGO, isShowed).apply();
    }

    public static boolean getRemoveLogoShowed(Context context) {
        return getPreferences(context).getBoolean(Key.REMOVE_LOGO, false);

    }
}