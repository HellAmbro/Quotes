package com.rutershok.quotes.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Storage;

public class Ad {

    private static InterstitialAd mInterstitialAd;
    private static RewardedVideoAd mRewardedVideoAd;
    private static AdRequest mAdRequest;

    private static void getConsentStatus(final Activity activity) {
        if (!activity.isDestroyed() && !activity.isFinishing()) {
            ConsentInformation.getInstance(activity).requestConsentInfoUpdate(new String[]{Key.ADMOB_PUB_ID}, new ConsentInfoUpdateListener() {
                @Override
                public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                    // User's consent status successfully updated.
                    switch (consentStatus) {
                        case UNKNOWN:
                            Dialog.gdprConsent(activity);
                            break;
                        case NON_PERSONALIZED:
                            ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.NON_PERSONALIZED);
                            break;
                        case PERSONALIZED:
                           ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.PERSONALIZED);
                            break;
                    }
                }

                @Override
                public void onFailedToUpdateConsentInfo(String errorDescription) {
                    // User's consent status failed to update.
                }
            });
        }
    }
    public static void initialize(final Activity activity) {
        if (!Storage.getPremium(activity)) {
            getConsentStatus(activity);

            MobileAds.initialize(activity, Key.ADMOB_APP_ID);

            getAdRequest();
            getInterstitialAd(activity);
        }
    }


    private static AdRequest getAdRequest() {
        if (mAdRequest == null) {
            mAdRequest = new AdRequest.Builder().build();
        }
        return mAdRequest;
    }

    private static InterstitialAd getInterstitialAd(Context context) {
        if (mInterstitialAd == null) {
            mInterstitialAd = new InterstitialAd(context);
            mInterstitialAd.setAdUnitId(Key.ADMOB_INTERSTITIAL_ID);
            mInterstitialAd.loadAd(getAdRequest());
        }
        return mInterstitialAd;
    }

    private static RewardedVideoAd getRewardedVideoAd(Activity activity) {
        if (mRewardedVideoAd == null) {
            mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(activity);
        }
        return mRewardedVideoAd;
    }

    public static void showBanner(Activity activity) {
        if (!Storage.getPremium(activity)) {
            final AdView bannerView = activity.findViewById(R.id.ads_banner);
            bannerView.loadAd(getAdRequest());
            bannerView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    bannerView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public static void showInterstitialWithFrequency(Activity activity) {
        if (!Storage.getPremium(activity) &&
                Storage.getInterstitialCount(activity) % Key.INTERSTITIAL_FREQUENCY == 0) {
            showInterstitial(activity);
        }
        Storage.updateInterstitialCount(activity);
    }

    public static void showInterstitial(final Context context) {
        if (!Storage.getPremium(context)) {
            if (getInterstitialAd(context) != null) {
                getInterstitialAd(context).show();
                getInterstitialAd(context).setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        getInterstitialAd(context).loadAd(getAdRequest());
                    }
                });
            } else {
                getInterstitialAd(context);
            }
        }
    }

    public static void showRemoveLogo(@NonNull final Activity activity, @NonNull final View view) {
            getRewardedVideoAd(activity).loadAd(Key.ADMOB_REWARDED_VIDEO_ID, getAdRequest());
            getRewardedVideoAd(activity).setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewardedVideoAdLoaded() {
                    getRewardedVideoAd(activity).show();
                }

                @Override
                public void onRewardedVideoAdOpened() {
                }

                @Override
                public void onRewardedVideoStarted() {
                }

                @Override
                public void onRewardedVideoAdClosed() {
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    view.setVisibility(View.GONE);
                }

                @Override
                public void onRewardedVideoAdLeftApplication() {
                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {
                    Snackbar.showText(activity, R.string.video_failed_to_load);
                }

                @Override
                public void onRewardedVideoCompleted() {
                }
            });
    }
}