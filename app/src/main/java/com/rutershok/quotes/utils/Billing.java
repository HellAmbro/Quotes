package com.rutershok.quotes.utils;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.rutershok.quotes.models.Premium;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Storage;

public class Billing {

    private static BillingProcessor mBillingProcessor;

    public static void initialize(final Activity activity) {
        try {
            mBillingProcessor = new BillingProcessor(activity, Key.GOOGLE_PLAY_KEY, Key.MERCHANT_ID, new BillingProcessor.IBillingHandler() {
                @Override
                public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
                    Snackbar.showText(activity, R.string.product_correctly_purchased);
                    Storage.setPremium(activity, true);
                }

                @Override
                public void onPurchaseHistoryRestored() {
                }

                @Override
                public void onBillingError(int errorCode, @Nullable Throwable error) {
                    //Toast.makeText(activity, "Billing Error with code " + errorCode, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onBillingInitialized() {
                    if (mBillingProcessor != null && mBillingProcessor.isInitialized()) {
                        for (Premium premium : Premium.values()) {
                            try {
                                if (mBillingProcessor.getPurchaseListingDetails(premium.nameId).priceLong != 0) {
                                    Storage.setPremiumPrice(activity, premium.nameId, mBillingProcessor.getPurchaseListingDetails(premium.nameId).priceText);
                                } else {
                                    Storage.setPremiumPrice(activity, premium.nameId, "N.A.");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        for (Premium premium : Premium.values()) {
                            try {
                                if (mBillingProcessor.isPurchased(premium.nameId)) {
                                    Storage.setPremium(activity, true);
                                    return;
                                } else {
                                    Storage.setPremium(activity, false);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void purchasePremium(final Activity activity, String nameId) {
        if (mBillingProcessor != null && mBillingProcessor.isInitialized()) {
            mBillingProcessor.purchase(activity, nameId);
        } else {
            Snackbar.showText(activity, R.string.error_billing);
            initialize(activity);
        }
    }
}