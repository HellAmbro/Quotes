package com.rutershok.quotes.utils;


import static android.graphics.Color.WHITE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.rutershok.quotes.EditorActivity;
import com.rutershok.quotes.R;
import com.rutershok.quotes.adapters.SocialsAdapter;
import com.rutershok.quotes.models.Category;
import com.rutershok.quotes.models.Quote;
import com.rutershok.quotes.storage.Database;
import com.rutershok.quotes.storage.Storage;
import com.xw.repo.BubbleSeekBar;

import org.json.JSONException;
import org.json.JSONObject;

public class Dialog {

    private static GradientDrawable gradientDrawable;

    private static int gradientColorTop = WHITE;
    private static int gradientColorBottom = WHITE;

    public static void quoteOfTheDay(final Context context) {
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_quote_of_the_day, null, false);
        final TextView textCategory = view.findViewById(R.id.text_category);
        final TextView textAuthor = view.findViewById(R.id.text_author);
        final TextView textQuote = view.findViewById(R.id.text_quote);

        String url = Key.API_URL + "get_daily_quote.php?lang=" + Storage.getLanguage(context);
        Log.e("url", url);

        Database.getRequestQueue(context).add(new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONObject object = new JSONObject(response).getJSONArray(Key.QUOTES).getJSONObject(0);
                        final Quote quote = new Quote();
                        quote.setQuote(object.getString(Key.QUOTE));
                        quote.setAuthor(object.getString(Key.AUTHOR));
                        quote.setCategory(object.getString(Key.CATEGORY));
                        textCategory.setText(Category.translate(context, quote.getCategory()));
                        textQuote.setText(quote.getQuote());
                        textAuthor.setText(String.format("- %s", quote.getAuthor()));
                        view.findViewById(R.id.cl_quote_of_the_day).setOnClickListener(v -> context.startActivity(new Intent(context, EditorActivity.class)
                                .putExtra(Key.QUOTE, quote.getQuote())
                                .putExtra(Key.AUTHOR, quote.getAuthor())));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        new AlertDialog.Builder(context).setView(view).show();
                    }
                }, error -> {
                }));

    }

    public static void textColor(final Context context, final TextView textView) {
        final int currentColor = textView.getCurrentTextColor();

        ColorPickerDialogBuilder
                .with(context)
                .setTitle(context.getString(R.string.choose_color))
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(8)
                .setOnColorChangedListener(selectedColor -> {
                    textView.setTextColor(selectedColor);//Set new color
                })
                .setPositiveButton(android.R.string.yes, (dialog, selectedColor, allColors) -> {
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> textView.setTextColor(currentColor))
                .build()
                .show();
    }

    public static void textSize(Context context, final TextView textView) {
        final float currentSize = textView.getTextSize() / context.getResources().getDisplayMetrics().scaledDensity;
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_seek_bar, null, false);
        BubbleSeekBar seekBar = view.findViewById(R.id.seek_bar_editor);
        seekBar.setProgress(currentSize);
        seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                textView.setTextSize((float) (progress));
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(android.R.string.yes, null)
                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> textView.setTextSize(currentSize)).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    public static void textOpacity(Context context, final TextView textView) {
        final float currentAlpha = textView.getAlpha();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_seek_bar, null, false);
        BubbleSeekBar seekBar = view.findViewById(R.id.seek_bar_editor);
        seekBar.setProgress(currentAlpha * 100);
        seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                textView.setAlpha((float) (progress * 0.01));
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(android.R.string.yes, null)
                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> textView.setAlpha(currentAlpha)).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    public static void textSpacing(Context context, final TextView textView) {
        final float currentSpacing = textView.getLineSpacingMultiplier();
        Log.e("Spacing", currentSpacing + "");
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_seek_bar, null, false);
        BubbleSeekBar seekBar = view.findViewById(R.id.seek_bar_editor);
        seekBar.setProgress((float) (currentSpacing / 0.02));
        seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                if (progress == 0) {
                    progress = 1;
                }
                textView.setLineSpacing(0, (float) (progress * 0.02));
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(android.R.string.yes, null)
                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> textView.setLineSpacing(0, currentSpacing)).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    public static void textHighlight(final Context context, final TextView textView) {
        final Drawable currentBackgroundColor = textView.getBackground();
        ColorPickerDialogBuilder
                .with(context)
                .setTitle(context.getString(R.string.choose_color))
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(8)
                .setOnColorChangedListener(selectedColor -> textView.setBackground(new ColorDrawable(selectedColor)))
                .setPositiveButton(android.R.string.yes, (dialog, selectedColor, allColors) -> {
                })
                .setNegativeButton(android.R.string.cancel, (dialog, which) -> textView.setBackground(currentBackgroundColor))
                .build()
                .show();
    }

    public static void textBorder(Context context, final TextView textView) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_seek_bar, null, false);
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        final int currentMargin = layoutParams.leftMargin;
        BubbleSeekBar seekBar = view.findViewById(R.id.seek_bar_editor);
        seekBar.setProgress(currentMargin / 4);
        seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(textView.getLayoutParams());
                int margin = progress * 4;
                marginParams.setMargins(margin, 0, margin, 0);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                textView.setLayoutParams(layoutParams);
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(android.R.string.yes, null)
                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> {
                    ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(textView.getLayoutParams());
                    marginParams.setMargins(currentMargin, 0, currentMargin, 0);
                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(marginParams);
                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                    textView.setLayoutParams(layoutParams1);
                }).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    public static void textEditQuote(Context context, final TextView textQuote) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.dialog_edit_quote, null);
        final TextInputEditText editTextQuote = view.findViewById(R.id.edit_text_quote);
        editTextQuote.setText(textQuote.getText());
        new AlertDialog.Builder(context).setView(view).setPositiveButton(android.R.string.yes, (dialog, which) -> {
            if (editTextQuote.getText() != null) {
                textQuote.setText(editTextQuote.getText().toString());
            }
        }).setNegativeButton(android.R.string.cancel, null).show();
    }

    public static void backgroundColor(final Context context, final ImageView imageView) {
        final Drawable currentBackgroundDrawable = imageView.getDrawable();
        try {
            ColorPickerDialogBuilder
                    .with(context)
                    .setTitle(context.getString(R.string.choose_color))
                    .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                    .density(8)
                    .setOnColorChangedListener(selectedColor -> Glide.with(context).load(new ColorDrawable(selectedColor)).into(imageView))
                    .setPositiveButton(android.R.string.yes, (dialogInterface, i, integers) -> {

                    })
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> Glide.with(context).load(currentBackgroundDrawable).into(imageView))
                    .build()
                    .show();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void backgroundGradient(final Context context, final ImageView imageView) {
        final Drawable currentBackgroundDrawable = imageView.getDrawable();
        gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(0f);
        gradientDrawable.setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
        try {
            //Bottom color
            ColorPickerDialogBuilder
                    .with(context)
                    .setTitle(context.getString(R.string.choose_color))
                    .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                    .density(8)
                    .setOnColorChangedListener(selectedColor -> {
                        gradientColorBottom = selectedColor;
                        gradientDrawable.setColors(new int[]{gradientColorBottom, gradientColorTop});
                        Glide.with(context).load(gradientDrawable).into(imageView);
                    })
                    .setPositiveButton(android.R.string.yes, (dialog, selectedColor, allColors) -> {
                        //Top color
                        ColorPickerDialogBuilder
                                .with(context)
                                .setTitle(context.getString(R.string.choose_color))
                                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                                .density(8)
                                .setOnColorChangedListener(selectedColor1 -> {
                                    gradientColorTop = selectedColor1;
                                    gradientDrawable.setColors(new int[]{gradientColorBottom, gradientColorTop});
                                    Glide.with(context).load(gradientDrawable).into(imageView);
                                })
                                .setPositiveButton(android.R.string.yes, (dialogInterface, i, integers) -> {
                                })
                                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> Glide.with(context).load(currentBackgroundDrawable).into(imageView))
                                .build()
                                .show();
                    })
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> Glide.with(context).load(currentBackgroundDrawable).into(imageView))
                    .build()
                    .show();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void backgroundOpacity(Context context, final ImageView imageView) {
        final float currentAlpha = imageView.getAlpha();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_seek_bar, null, false);
        BubbleSeekBar seekBar = view.findViewById(R.id.seek_bar_editor);
        seekBar.setProgress(currentAlpha * 100);
        seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {
                imageView.setAlpha((float) (progress * 0.01));
            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(android.R.string.yes, null)
                .setNegativeButton(android.R.string.cancel, (dialog1, which) -> imageView.setAlpha(currentAlpha)).create();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    public static void gdprConsent(final Activity activity) {
        try {
            if (!activity.isFinishing()) {
                View view = LayoutInflater.from(activity).inflate(R.layout.dialog_eu_consent, null, false);

                new AlertDialog.Builder(activity)
                        .setView(view)
                        .setTitle(R.string.data_protection_consent)
                        .setIcon(R.drawable.ic_gdpr)
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.PERSONALIZED))
                        .setNegativeButton(android.R.string.no, (dialog, which) -> ConsentInformation.getInstance(activity).setConsentStatus(ConsentStatus.NON_PERSONALIZED)).show();
            }
        } catch (Exception e) {
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public static void noFavoriteQuotes(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.you_haven_t_yet_added_quotes_to_favorites)
                .setPositiveButton(android.R.string.ok, null).show();
    }

    public static void myImagesIsEmpty(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.you_have_no_saved_images)
                .setMessage(R.string.saved_images_instruction).setPositiveButton(android.R.string.ok, null).show();
    }

    public static void showRemoveLogo(final Context context) {
        if (!Storage.getRemoveLogoShowed(context) && !Storage.getPremium(context)) {
            new android.app.AlertDialog.Builder(context)
                    .setTitle(R.string.remove_logo)
                    .setMessage(R.string.remove_logo_instruction)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            Storage.setRemoveLogoShowed(context, true);
        }
    }

    public static class ShareBottomSheet extends BottomSheetDialog {

        final Activity mActivity;

        public ShareBottomSheet(@NonNull Activity activity) {
            super(activity);
            this.mActivity = activity;
            create();
        }

        public void create() {
            View view = getLayoutInflater().inflate(R.layout.bottom_sheet_share, null);
            setContentView(view);
            setShareWithSocial(view);
            TabLayout tabLayout = view.findViewById(R.id.tab_layout_share);
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    switch (tab.getPosition()) {
                        case 0:
                            Share.withImage(mActivity);
                            break;
                        case 1:
                            Share.withText(mActivity, ((TextView) mActivity.findViewById(R.id.text_image_quote)).getText().toString());
                            break;
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    switch (tab.getPosition()) {
                        case 0:
                            Share.withImage(mActivity);
                            break;
                        case 1:
                            Share.withText(mActivity, ((TextView) mActivity.findViewById(R.id.text_image_quote)).getText().toString());
                            break;
                    }
                }
            });
        }

        private void setShareWithSocial(View view) {
            RecyclerView recyclerView = view.findViewById(R.id.recycler_socials);
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(new SocialsAdapter(mActivity));
        }
    }

    /*public static void rateThisApp(Activity activity) {
        RateThisApp.onCreate(activity);
        RateThisApp.showRateDialogIfNeeded(activity);
    }*/
}