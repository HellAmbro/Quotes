package com.rutershok.quotes.utils;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.rutershok.quotes.fragments.QuotesFragment;
import com.rutershok.quotes.R;

public class Fragments {

    public static void replaceWithHistory(Context context, String title, String query) {
        Fragment fragment = new QuotesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Key.TITLE, title);
        bundle.putString(Key.QUERY, query);
        fragment.setArguments(bundle);
        ((FragmentActivity) context).getSupportFragmentManager().beginTransaction().addToBackStack(Key.HISTORY).replace(R.id.container, fragment).commit();
    }

    public static void replaceWithHistory(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().addToBackStack(Key.HISTORY).replace(R.id.container, fragment).commit();
    }
}
