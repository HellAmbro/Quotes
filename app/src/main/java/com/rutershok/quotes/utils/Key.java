package com.rutershok.quotes.utils;

public final class Key {
    public static final int GET_FROM_GALLERY = 3;
    public static final int INTERSTITIAL_FREQUENCY = 20;
    public static final int LIMIT = 30;
    public static final String ADMOB_APP_ID = "ca-app-pub-9514195433111172~5403914937", ADMOB_INTERSTITIAL_ID = "ca-app-pub-9514195433111172/6484559670";
    public static final String ADMOB_PUB_ID = "pub-9514195433111172";
    public static final String API_URL = "http://quotes2018.netsons.org/api/";
    public static final String AUTHOR = "Author";
    public static final String CATEGORY = "Category";
    public static final String EMAIL_ADDRESS = "rutershok@gmail.com";
    public static final String FACEBOOK_ID = "1764033000524766";
    public static final String FAVORITE_QUOTES = "Favorite Quotes";
    public static final String GOOGLE_PLAY_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAolrlFGGjshTd5dJzJLD5s6Sr6oSj/urD0lOaYi7Eb/20H9r1GWyGaJVlyLugx1Wl2/lmWx253djxc9lQqXEvqTfAbn1FK1G2SKhU7dVEo+kfYEj8zTebDGymzPVTPVhQ4bMN1zZZv5gmEJs9R2R8JcGI3xTcy6mHPDM70bBDzzV1i43RtSy6QpKXRPKdCstx3WkxNKTSpQYvhU9cXJ0wcj/xpOHoPKC659fzdvulPBKdmJlteRYsfBHt4UoF2actbFL0fQe/oN5+BGMAIVfo1y5SXBBPVXoJZ4S7379j//14LTipUjB8ZhRVqF4WQyhu9oJYYK3II24+uRA9bjIllwIDAQAB";
    public static final String HISTORY = "history";
    public static final String HOUR = "Hour";
    public static final String INTERSTITIAL_COUNT = "Interstitial Count";
    public static final String MERCHANT_ID = "7246-0306-7253";
    public static final String MINUTE = "Minute";
    public static final String PREF_CONTACT_US = "pref_contact_us";
    public static final String PREF_FACEBOOK = "pref_facebook";
    public static final String PREF_GDPR = "pref_gdpr";
    public static final String PREF_GOOGLE_PLAY = "pref_google_play";
    public static final String PREF_INSTAGRAM = "pref_instagram";
    public static final String PREF_NOTIFICATIONS = "pref_notifications";
    public static final String PREF_PRIVACY_POLICY = "pref_privacy_policy";
    public static final String PREF_QUOTE_OF_THE_DAY_TIME = "pref_quote_of_the_day_time";
    public static final String PREF_RATE_APP = "pref_rate_app";
    public static final String PREF_SHARE_APP = "pref_share_app";
    public static final String PREF_TWITTER = "pref_twitter", PREF_INVITE = "pref_invite";
    public static final String PREMIUM = "Premium";
    public static final String PREMIUM_BURGER = "premium_hamburger";
    public static final String PREMIUM_CHIPS = "premium_chips";
    public static final String PREMIUM_ICE_CREAM = "premium_ice_cream";
    public static final String PREMIUM_PIZZA = "premium_pizza";
    public static final String QUERY = "Query";
    public static final String QUOTE = "Quote";
    public static final String QUOTES = "Quotes";
    public static final String RUTERSHOK = "rutershok";
    public static final String TITLE = "Title";
    public static final String TWITTER_ID = "840671248816492544";

    public static final String[] PREFERENCES = {
            PREF_NOTIFICATIONS,
            //PREF_QUOTE_OF_THE_DAY_TIME,
            PREF_CONTACT_US,
            PREF_INVITE,
            PREF_RATE_APP,
            PREF_SHARE_APP,
            PREF_GOOGLE_PLAY,
            PREF_INSTAGRAM,
            PREF_FACEBOOK,
            PREF_TWITTER,
            PREF_PRIVACY_POLICY,
            PREF_GDPR};
    public static final int INVITE_REQUEST = 99;

    public static final String PARENT_PATH = "/storage/emulated/0/";
    public static final String LANGUAGE = "Language";
    public static final String ADMOB_REWARDED_VIDEO_ID = "ca-app-pub-9514195433111172/4782249126";
    public static final String REMOVE_LOGO = "RemoveLogo";

    public static final String DAILY_PACKAGE = "com.rutershok.daily";
    public static final String PHRASES_PACKAGE = "com.rutershok.phrases";
    public static final String UNSPLASH = "4d3ee8b5054e1d8168d7080b78e47b16cd7464c25f285a6e393c9c72c26860ff";
}