package com.rutershok.quotes.utils;


import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rutershok.quotes.MainActivity;
import com.rutershok.quotes.R;
import com.rutershok.quotes.receiver.NotificationsReceiver;
import com.rutershok.quotes.storage.Database;
import com.rutershok.quotes.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

public class Notifications {

    public static void initialize(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationsReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        // Set the alarm
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Storage.getQuoteOfTheDayHour(context));
        calendar.set(Calendar.MINUTE, Storage.getQuoteOfTheDayMinute(context));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        // setRepeating() lets you specify a precise custom interval--in this case,1 day
        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    public static void send(final Context context) {
        Database.getRequestQueue(context).add(new StringRequest(Request.Method.GET, Key.API_URL + "get_daily_quote.php?lang="+Locale.getDefault().getLanguage(),
                response -> {
                    try {
                        create(context, context.getResources().getString(R.string.quote_of_the_day),
                                new JSONObject(response)
                                        .getJSONArray(Key.QUOTES)
                                        .getJSONObject(0)
                                        .getString(Key.QUOTE));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                }));
    }

    //This method generate notification
    private static void create(Context context, String messageTitle, String messageBody) {
        // Notification channel ( >= API 26 )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(new NotificationChannel("QUOTES_CHANNEL",context.getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH));
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "QUOTES_CHANNEL")
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_quote)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class)/*.putExtra("fromNotification", true)*/, PendingIntent.FLAG_UPDATE_CURRENT))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}
