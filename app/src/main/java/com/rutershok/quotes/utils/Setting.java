package com.rutershok.quotes.utils;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.TimePicker;

import androidx.preference.Preference;
import androidx.preference.SwitchPreference;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.rutershok.quotes.R;
import com.rutershok.quotes.storage.Storage;

public class Setting {

    public static void openPublisherPage(Context context) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Rutershok")));
    }

    public static void openAppPage(Context context) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
    }

    public static void openInstagram(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/_u/" + Key.RUTERSHOK)).setPackage("com.instagram.android"));
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + Key.RUTERSHOK)));
        }
    }

    public static void openFacebook(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + Key.FACEBOOK_ID)));
        } catch (Exception e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/" + Key.RUTERSHOK)));
        }
    }

    public static void openTwitter(Context context) {
        Uri uri;
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            uri = Uri.parse("twitter://user?user_id=" + Key.TWITTER_ID);
        } catch (Exception e) {
            uri = Uri.parse("https://twitter.com/" + Key.RUTERSHOK);
        }
        context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    public static void openPrivacyPolicy(Context context) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://rutershok.netsons.org/privacy.html")));
    }

    public static void contactUs(Context context) {
        context.startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + Key.EMAIL_ADDRESS)), "Send mail"));
    }

    public static void shareApp(Context context) {
        Intent intent = new Intent().setAction(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, context.getString(R.string.download_this_app)).setType("text/plain");
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_this_app)));
    }

    public static void setNotifications(Preference preference) {
        if (((SwitchPreference) preference).isChecked()) {
            Storage.setNotifications(preference.getContext(), true);
        } else {
            Storage.setNotifications(preference.getContext(), false);
        }
    }

    public static void setTimePicker(final Context context) {
            TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
                Storage.setQuoteOfTheDayHour(context, hourOfDay);
                Storage.setQuoteOfTheDayMinute(context, minute);
                Notifications.initialize(context);
            }, Storage.getQuoteOfTheDayHour(context), Storage.getQuoteOfTheDayMinute(context), true);
            timePickerDialog.show();
    }

    public static void inviteFriends(Activity activity) {
        Intent intent = new AppInviteInvitation.IntentBuilder(activity.getString(R.string.invite_your_friends))
                .setMessage(activity.getString(R.string.download_this_app))
                .build();
        activity.startActivityForResult(intent, Key.INVITE_REQUEST);
    }
}