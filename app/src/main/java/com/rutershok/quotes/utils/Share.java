package com.rutershok.quotes.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.rutershok.quotes.R;
import com.rutershok.quotes.models.Social;
import com.rutershok.quotes.storage.Storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pub.devrel.easypermissions.EasyPermissions;

public class Share {

    public static void copyToClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(context.getResources().getString(R.string.quotes),
                text + " " + context.getString(R.string.download_this_app));
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
    }

    public static void withText(final Context context, final String quoteText) {
        new Thread(() -> {
            StringBuilder quoteBuilder = new StringBuilder(quoteText);
            if (!Storage.getPremium(context)) {
                quoteBuilder.append("\n\n").append(context.getString(R.string.download_this_app));
            }
            context.startActivity(Intent.createChooser(new Intent().setAction(Intent.ACTION_SEND).putExtra(Intent.EXTRA_TEXT, quoteBuilder.toString()).setType("text/plain"), context.getString(R.string.share)));
        }).start();
    }


    private static Bitmap getBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getHeight(), view.getHeight(), Bitmap.Config.ARGB_8888);
        view.layout(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        view.draw(new Canvas(bitmap));
        return Bitmap.createScaledBitmap(bitmap, view.getWidth(),view.getHeight(), true);
    }

    public static void launchApp(Context context, String packageName) {
        try {
            context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(packageName));
        } catch (PackageManager.NameNotFoundException e) {
            openAppPage(context, packageName);
        }
    }

    private static void openAppPage(Context context, String packageName) {
        context.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + packageName)));
    }

    public static void withImage(final Activity activity) {
        final Bitmap bitmap = getBitmap(activity.findViewById(R.id.rl_image));
        new Thread(() -> {
            try {
                File cachePath = new File(activity.getCacheDir(), "images");
                if (!cachePath.exists()) {
                    cachePath.mkdirs();
                } // don't forget to make the directory
                FileOutputStream outputStream = new FileOutputStream(cachePath + "/image.png");
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                File newFile = new File(new File(activity.getCacheDir(), "images"), "image.png");
                Uri uri = FileProvider.getUriForFile(activity, "com.rutershok.quotes.fileprovider", newFile);
                if (uri != null) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND)
                            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            .setDataAndType(uri, activity.getContentResolver().getType(uri))
                            .putExtra(Intent.EXTRA_STREAM, uri);
                    activity.startActivity(Intent.createChooser(shareIntent, ""));
                }
                if (!bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
        }).start();

    }

    public static void withSocial(Activity activity, Social social) {
        try {
            File cachePath = new File(activity.getCacheDir(), "images");
            if (!cachePath.exists()) {
                cachePath.mkdirs();
            }
            FileOutputStream outputStream = new FileOutputStream(cachePath + "/image.png");
            getBitmap(activity.findViewById(R.id.rl_image)).compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            File newFile = new File(new File(activity.getCacheDir(), "images"), "image.png");
            Uri uri = FileProvider.getUriForFile(activity, "com.rutershok.quotes.fileprovider", newFile);
            if (uri != null) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND)
                        .putExtra(Intent.EXTRA_STREAM, uri)
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        .setType("image/png");
                if (activity.getPackageManager().getLaunchIntentForPackage(social.packageName) != null) {
                    shareIntent.setPackage(social.packageName);
                    activity.startActivity(Intent.createChooser(shareIntent, ""));
                } else {
                    Toast.makeText(activity, R.string.app_not_installed, Toast.LENGTH_LONG).show();
                }
            } else {
                Log.e("Uri", "Null");
            }
        }
    }
    public static void saveImage(final Activity activity) {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            final View view = activity.findViewById(R.id.rl_image);
            final File directory = new File(Key.PARENT_PATH, activity.getString(R.string.app_name));
            // Create a storage directory if it does not exist
            if (!directory.exists()) {
                directory.mkdirs();
            }
            // Create a media file name
            String selectedOutputPath = directory.getPath() + File.separator + "quote_" + System.currentTimeMillis() + ".png";
            File file = new File(selectedOutputPath);
            try {
                Bitmap bitmap = getBitmap(view);
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
                if (!bitmap.isRecycled()) {
                    bitmap.recycle();
                    Log.e("Bitmap", "Recycled");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Snackbar.showText(activity, R.string.image_saved);
            Ad.showInterstitial(activity);
            //Scan files
            activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,Uri.fromFile(new File(directory.getAbsolutePath()))));
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.rationale_ask), 11, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public static void withMyImage(View view, String imageName) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Key.PARENT_PATH + view.getContext().getString(R.string.app_name) + "/" + imageName));
        sendIntent.setType("image/*");
        view.getContext().startActivity(Intent.createChooser(sendIntent, view.getContext().getString(R.string.share_image)));
        Ad.showInterstitial(view.getContext());
    }
}
