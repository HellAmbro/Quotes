package com.rutershok.quotes.utils;


import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.rutershok.quotes.R;

import de.mateware.snacky.Snacky;

public class Snackbar {

    public static void networkUnavailable(Activity activity, View.OnClickListener clickListener) {
        try {
            Snacky.builder().setActivity(activity)
                    .setTextSize(16)
                    .setTextColor(Color.WHITE)
                    .setActionTextColor(Color.WHITE)
                    .setDuration(Snacky.LENGTH_LONG)
                    .setText(R.string.network_unavailable)
                    .setActionText(R.string.reload)
                    .setActionClickListener(clickListener)
                    .error()
                    .show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static void deleteImage(View view, View.OnClickListener clickListener) {
        try {
            Snacky.builder().setView(view)
                    .setTextSize(16)
                    .setTextColor(Color.WHITE)
                    .setActionTextColor(Color.WHITE)
                    .setDuration(Snacky.LENGTH_LONG)
                    .setText(R.string.delete_image)
                    .setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                    .setActionText(android.R.string.yes)
                    .setActionClickListener(clickListener)
                    .build()
                    .show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static void buyPremiumVersion(Activity activity, final FragmentManager fragmentManager, final Fragment fragment) {
        try {
            Snacky.builder()
                    .setActivity(activity)
                    .setTextSize(16)
                    .setActionText(R.string.purchase)
                    .setActionTextColor(Color.WHITE)
                    .setActionClickListener(v -> Fragments.replaceWithHistory(fragmentManager, fragment))
                    .setTextColor(Color.WHITE)
                    .setActionTextColor(Color.WHITE)
                    .setDuration(Snacky.LENGTH_LONG)
                    .setText(R.string.you_need_premium_version)
                    .setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                    .build()
                    .show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static void showText(Activity activity, int stringResId) {
        try {
            Snacky.builder()
                    .setActivity(activity)
                    .setTextSize(16)
                    .setTextColor(Color.WHITE)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setText(activity.getString(stringResId))
                    .setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                    .build()
                    .show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static void showText(View view, int stringResId) {
        try {
            Snacky.builder()
                    .setView(view)
                    .setTextSize(16)
                    .setTextColor(Color.WHITE)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setText(view.getContext().getString(stringResId))
                    .setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary))
                    .build()
                    .show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
